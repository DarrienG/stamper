# STAMPER The simple as it gets timestamp converter

How often do you go to the web to type in `unix time to date converter` or
something like that?

Not very often? It's just me? Eh

Well if you do a lot, it's a real pain. I'm just trying to convert a number to
my local date and UTC. I don't want 2 megabytes of Javascript fired at my face.

Trying to find a solution in the terminal is a nuisance too. Use date with some
formatting specifiers and it'll output a date. But was that in your timezone or
UTC? Was it %S or %n again? What if I want to do seconds instead of millis?

Then if you're working on macOS you can throw that all out the window.
Crossplatform solutions usually involve invoking Python and writing a one line
script to do it for you. Slow to type, and a pain to modify, this is not a
solution.

I don't even know about Windows. I don't care much about it either.

## What's the solution?

I wrote `stamper` just for this purpose. I work with timestamps more than I care
to admit sometimes. I just want a date from time. Don't make me work for it.

Looking at the timestamps I get, half the time I don't know if it's in
nanoseconds, milliseconds, or seconds. Yet again though, why should I have to
know? Too much work, just figure it out for me.

And so `stamper` does that.

Check it out:

```bash
$ stamper 1581472087907000000
Wed, 12 Feb 2020 01:48:07 +0000
Tue, 11 Feb 2020 20:48:07 -0500
```

Is it in Nanoseconds? Milliseconds? Seconds? Do I care? No. But `stamper` does,
and gives me the right date anyway.

If you don't trust the algorithm though, you can force a unit:

```bash
$ stamper -s 1581472087907
Wed, 08 Nov 52084 02:11:47 +0000
Tue, 07 Nov 52084 21:11:47 -0500
```

Congrats, it's 52084 now. Is this really what you wanted?

If you give it crazy input or bad flags, `stamper` will freak out. This is your
problem though. It only works about 50000 years in the future. Make it work.

Stamper supports all sorts of units:

```bash
$ stamper -h
stamper 0.0.1
Darrien Glasser <me@darrien.dev>
Converts unix seconds to dates

USAGE:
    stamper [FLAGS] <STAMP>

FLAGS:
    -m, --millis     Assume input is in milliseconds
    -n, --nanos      Assume input is in nanoseconds
    -s, --seconds    Assume input is in seconds
    -h, --help       Prints help information
    -V, --version    Prints version information

ARGS:
    <STAMP>    Timestamp to get the date value of
```

To download, check out the [releases
page](https://gitlab.com/DarrienG/stamper/-/releases), or run `cargo install
stamper`
