use chrono::{Local, TimeZone, Utc};

const SECOND_END: i64 = 100_000_000_000 - 1;
const MILLI_START: i64 = 100_000_000_000;
const MILLI_END: i64 = 100_000_000_000_000_000 - 1;

pub fn intelligent_date(stamp: i64) -> String {
    match stamp {
        0..=SECOND_END => date_from_seconds(stamp),
        MILLI_START..=MILLI_END => date_from_millis(stamp),
        _ => date_from_nanos(stamp),
    }
}

pub fn date_from_seconds(stamp: i64) -> String {
    format!(
        "{}\n{}",
        Utc.timestamp(stamp, 0).to_rfc2822(),
        Local.timestamp(stamp, 0).to_rfc2822(),
    )
}

pub fn date_from_millis(stamp: i64) -> String {
    format!(
        "{}\n{}",
        Utc.timestamp_millis(stamp).to_rfc2822(),
        Local.timestamp_millis(stamp).to_rfc2822()
    )
}

pub fn date_from_nanos(stamp: i64) -> String {
    format!(
        "{}\n{}",
        Utc.timestamp_nanos(stamp).to_rfc2822(),
        Local.timestamp_nanos(stamp).to_rfc2822()
    )
}
