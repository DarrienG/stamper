use clap::{App, Arg};
use std::convert::TryInto;
use std::process;

mod stamper;

const SECONDS: &str = "SECONDS";
const MILLIS: &str = "MILLIS";
const NANOS: &str = "NANOS";

const SECONDS_CONFLICTS: [&str; 2] = [MILLIS, NANOS];
const MILLIS_CONFLICTS: [&str; 2] = [SECONDS, NANOS];
const NANOS_CONFLICTS: [&str; 2] = [SECONDS, MILLIS];

fn main() {
    let matches = App::new("stamper")
        .version("0.0.1")
        .about("Converts unix seconds to dates")
        .author("Darrien Glasser <me@darrien.dev>")
        .arg(
            Arg::with_name("STAMP")
                .required(true)
                .help("Timestamp to get the date value of")
                .takes_value(true),
        )
        .arg(
            Arg::with_name(SECONDS)
                .required(false)
                .takes_value(false)
                .short("s")
                .long("seconds")
                .conflicts_with_all(&SECONDS_CONFLICTS)
                .help("Assume input is in seconds"),
        )
        .arg(
            Arg::with_name(MILLIS)
                .required(false)
                .takes_value(false)
                .short("m")
                .long("millis")
                .conflicts_with_all(&MILLIS_CONFLICTS)
                .help("Assume input is in milliseconds"),
        )
        .arg(
            Arg::with_name(NANOS)
                .required(false)
                .takes_value(false)
                .short("n")
                .long("nanos")
                .conflicts_with_all(&NANOS_CONFLICTS)
                .help("Assume input is in nanoseconds"),
        )
        .get_matches();

    // we convert to u64 first to ensure positive value as well as valid timestamp
    let stamp = match matches.value_of("STAMP").unwrap().parse::<u64>() {
        Ok(v) => v,
        Err(_) => {
            eprintln!("Invalid input, must be valid timestamp");
            process::exit(1);
        }
    };

    // we convert to i64 afterwards because that's what chrono uses :unamused:
    let signed_stamp: i64 = stamp.try_into().unwrap();

    let seconds = matches.is_present(SECONDS);
    let millis = matches.is_present(MILLIS);
    let nanos = matches.is_present(SECONDS);

    println!(
        "{}",
        if seconds {
            stamper::date_from_seconds(signed_stamp)
        } else if millis {
            stamper::date_from_millis(signed_stamp)
        } else if nanos {
            stamper::date_from_nanos(signed_stamp)
        } else {
            stamper::intelligent_date(signed_stamp)
        }
    );
}
