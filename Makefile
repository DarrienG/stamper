release:
	cargo build --release
	strip target/release/stamper

check:
	cargo clippy --all -- -D warnings
	cargo test
	cargo fmt -- --check

fmt:
	cargo fmt
